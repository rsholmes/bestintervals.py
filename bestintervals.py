"""FIND best approximations to intervals in equal division or linear scales.
Print all scales in a range or only ones that improve the best score.
Score can be RMS of errors or sum of |errors| or maximum |error|.
Any number of intervals can be specified.
Any period and (for linear) generator can be specified.
For equal division, optimized fractional divisions can be requested.
"""

import argparse
from math import log, ceil, floor
import re

tab = False

def parse_ints (ai):
    ints = []
    intstr = []
    for aii in ai:
        if '/' in aii:
            [a, b] = aii.split('/')
            ints.append(1200*log(int(a)/int(b))/log(2))
            intstr.append(f"{a}/{b}")
        else:
            if aii[-1] == 'r':
                ints.append(1200*log(float(aii[:-1]))/log(2))
                intstr.append(f"{float(aii[:-1]):.4f}r")                
            else:
                ints.append(float(aii))
                intstr.append("")
    return [ints, intstr]

def score_calc(score_type, ea):
    nint = len(ea)
    if score_type == "a":
        return sum([abs(e) for e in ea])/nint
    elif score_type == "m":
        return max([abs(e) for e in ea])
    else:
        return(sum([e**2 for e in ea]))**0.5/nint

def pc (p):
    """Convert a string like '3/2' into cents"""
    [num, den] = [int(i) for i in p.split("/")]
    return 1200*log(num/den)/log(2)

def best (c, notes):
    """Given notes array of form [n, cn] find the cn closest to c and return [array n, cn]""" 
    beste = 9999
    besti = [0, 0.000]
    for i in range(len(notes)):
        if abs(notes[i][1]-c) < beste:
            beste = abs(notes[i][1]-c)
            besti = notes[i]
    return besti

def scale_dets (scale, dets):
    n = int(scale["n"])
    na = scale["na"]
    ca = scale["ca"]
    ea = scale["ea"]
    sg = scale["sg"]
    score = scale["sc"]
    e = scale["e"]
    fr = scale["fr"]
    per = scale["per"]

    # Notes in generated order
    notes = []
    if e:
        flat1 = 0
        for i in range (int(n)):
            notes.append([i, i*sg])
    else:
        flat1 = -ceil((n-7)/2) if n > 7 else 0
        for i in range (flat1, flat1+n):
            notes.append([i, (i*sg)%per])

    # Notes in pitch order
    onotes = sorted (notes, key = lambda x: x[1])
    # Notes indexed by generators
    gnotes = {nt[0]: nt[1] for nt in notes}
    gnotes2 = {nt[0]+n: nt[1] for nt in notes if nt[0] < 0}
    gnotes = { **gnotes, **gnotes2 }
    
    lim5 = {
        "P5": pc ("3/2"),
        "P4": pc ("4/3"),
        "M3": pc ("5/4"),
        "m3": pc ("6/5"),
        "M6": pc ("5/3"),
        "m6": pc ("8/5")
    }
    lim7 = {
        "7/6": pc ("7/6"),
        "7/5": pc ("7/5"),
        "7/4": pc ("7/4"),
        "8/7": pc ("8/7"),
        "10/7": pc ("10/7"),
        "12/7": pc ("12/7")
    }
    other = {
        "n3": pc ("11/9"),
        "n6": pc ("18/11")
    }

    diagen = []
    dodia = "d" in dets
    if e:
        for nt in onotes:
            if nt[1] > 1200*4/7 and nt[1] < 1200*3/5:
                diagen.append (nt[0])
            elif nt[1] >= 1200*3/5:
                break
    else:
        diagen = [1] if sg > 1200*4/7 and sg < 1200*3/5 else []

    if diagen == []:
        diagen = [-1]
        hasdia = False
    else:
        hasdia = True

    sep = "\t" if tab else " " # field separator
    sgs = "step" if e else "gen"
    if dets != "":
        print ()
        
    for dg in diagen:
        diatonic = {}
        if dodia:
            if hasdia:
                print (f"Diatonic scale with generator = {sep}{dg}{sep}{sgs+'s'}{sep}{dg*sg:<3.3f}{sep}cents")
                diatonic = {
                    "dia semi": gnotes[-5*dg % n],
                    "chr semi": gnotes[7*dg % n],
                    "dia whole": gnotes[(2*dg)%n],
                    "dia m3": gnotes[-3*dg % n],
                    "dia M3": gnotes[4*dg % n],
                    "dia P4": gnotes[-dg%n],
                    "dia P5": gnotes[dg],
                    "dia m6": gnotes[-4*dg % n],
                    "dia M6": gnotes[3*dg % n],
                    "dia LT": gnotes[5*dg % n]
                }

                if e:
                    dcw = []
                    for di in [["Diatonic semitone", -5], ["Chromatic semitone", 7], ["Whole tone", 2]]:
                        dsg = di[1]*dg % n
                        dcw.append(dsg)
                        print (f'{di[0]+" =":<20s}{sep}{dsg:>4d}{sep}steps{sep}{gnotes[dsg%n]:>8.3f}{sep}cents')
                else:
                    dcw = [-5, 7, 2]
                    dcw = []
                    for di in [["Diatonic semitone", -5], ["Chromatic semitone", 7], ["Whole tone", 2]]:
                        dsg = di[1] % n
                        dcw.append(dsg)
                        print (f'{di[0]+" =":<20s}{sep}{dsg:>4d}{sep}gens{sep}{gnotes[dsg%n]:>8.3f}{sep}cents')

                [bds, bcs, bwt] = dcw
                for di in [["m3", (bwt+bds)%n], ["M3", (2*bwt)%n], ["P5", (3*bwt+bds)%n]]:
                    dsg = di[1]
                    dsg -= n if not e and dsg > 7 + (n-7)/2 else 0
                    print (f'{"Diatonic "+di[0]+" =":<20s}{sep}{dsg:>4d}{sep}{sgs}s{sep}{gnotes[dsg%n]:>8.3f}{sep}cents{sep}err ={sep}{gnotes[dsg%n]-lim5[di[0]]:>8.3f}{sep}cents')
                print ()
            else:
                if e:
                    print ("No diatonic scales\n")
                else:
                    print ("Generator does not generate diatonic scale\n")

        if "n" in dets:
            ints = [lim5, diatonic, lim7, other]

            comma = "" if tab else ","
            lparen = "" if tab else "("
            rparen = "" if tab else ")"

            print (f"{sgs:>5s}{sep}{'Diff':>8s}{sep}{'Cents':>8s}{sep}{'Name' if hasdia else '':^8s}{sep}Intervals")
            prevnote = onotes[-1]
            for nt in onotes:
                diff = (nt[1] - prevnote[1]) % per
                prevnote = nt

                if hasdia:
                    # Find number of fifths from F
                    if e:
                        g2 = 1
                        s2 = 0
                        while s2 != nt[0]:
                            g2 += 1
                            if g2 == 1:
                                g2 = n
                                break
                            s2 = (s2 + dg) % n
                            if g2 >= 7+floor((n-7)/2):
                                g2 -= n
                    else:
                        g2 = nt[0]+1
                    if g2 >= n:
                        name = '-'
                    else:
                        name = "FCGDAEB"[g2%7]
                        if g2 > 0:
                            acc = '♯' * (g2//7)
                            acc = acc.replace ('♯♯', '𝄪')
                        else:
                            acc = '♭' * (-(g2+1)//7+1)
                            acc = acc.replace ('♭♭', '𝄫')
                        name += acc
                else:
                    name = ''

                nints = []
                for intsi in ints:
                    for intsj in intsi:
                        if best (intsi[intsj], notes)[0] == nt[0]:
                            nints.append ([intsj, nt[1]-intsi[intsj]])
                nints.sort (key=lambda x: abs(x[1]))
                print (f"{nt[0]:5d}{sep}{diff:8.3f}{sep}{nt[1]:8.3f}{sep}{name:^8s}", end="")
                commaif = ""
                for ni in nints:
                    if ni[0][0:3] == 'dia' or ni[0][0:3] == 'chr':
                        print (f"{commaif}{sep}{ni[0]}", end="")
                        commaif = f"{comma}{sep if tab else ''}"
                    else:
                        print (f"{commaif}{sep}{ni[0]}{sep}{lparen}{ni[1]:<3.3f}{rparen}", end="")
                        commaif = comma
                if tab and commaif != "":
                    print (commaif)
                else:
                    print ()
    
def main():

    parser = argparse.ArgumentParser()

    parser.add_argument (
        '-n', '--nsteps',
        type=int,
        nargs="+",
        default=[12,100],
        help='Min [and max] steps per octave, def = 12 100'
    )

    parser.add_argument (
        '-i', '--ints',
        type=str,
        nargs="+",
        default = ["3/2", "5/4"],
        help="Just intervals as fractions or decimals, def 3/2 5/4"
    )

    parser.add_argument (
        '-p', '--period',
        type=str,
        default="2/1",
        help="Period as fraction or decimals, def 2/1"
        )
    
    parser.add_argument (
        '-g', '--generator',
        type=str,
        nargs="+",
        default=["0"],
        help="Generator min, [max, [increment]] as fraction or decimals for linear, omit for EDO"
        )
    
    parser.add_argument (
        '-f', '--frac',
        type=int,
        default=1,
        help="(ET only) Do fractional divisions, FRAC per unit (default 1)"
        )

    parser.add_argument (
        '-s', '--scoretype',
        type=str,
        default='r',
        help="Score type: 'a' for average of |err| or 'm' for max(|err|) or r for RMS(err) (def)"
    )

    parser.add_argument (
        '-b', '--best',
        action="store_true",
        help="Show only best-scoring so far"
        )

    parser.add_argument (
        '-u', '--under',
        type=float,
        default=9999.,
        help="Show only if score is under UNDER"
        )

    parser.add_argument (
        '-o', '--opti',
        action="store_true",
        help="Optimize fractional divisions"
        )

    parser.add_argument (
        '-d', '--details',
        type=str,
        default="",
        help="Enable printing details about each scale: d = Diatonic scales info, n = List notes with their intervals"
        )

    parser.add_argument (
        '-t', '--tabs',
        action="store_true",
        help="Separate columns with tabs"
        )

    args = parser.parse_args()

    ai = args.ints
    aip = parse_ints(ai)
    if aip == None:
        print ("*** Invalid intervals specification")
        return
    [ints, intstr] = aip

    ap = [args.period]
    app = parse_ints(ap)
    if app == None or len(app[0]) != 1:
        print ("*** Invalid period specification")
        return
    [per, perstr] = [appi[0] for appi in app]

    if len(args.generator) > 3:
        print ("*** -g takes 1, 2, or 3 arguments")
        return
    if args.generator[0] == "0":
        typ = 'e'
        genmin = 1
        genmax = 1
        geninc = 1
        sgs = 'step'
    else:
        if args.frac > 1:
            print ("*** Cannot mix -f and -g")
            return
        typ = 'l'
        ag = args.generator
        agg = parse_ints(ag)
        if agg == None:
            print ("*** Invalid generator specification")
            return
        [genmin, genminstr] = [aggi[0] for aggi in agg]
        [genmax, genmaxstr] = [aggi[1] for aggi in agg] if len(agg[0])>1 else [genmin, genminstr]
        [geninc, genincstr] = [aggi[2] for aggi in agg] if len(agg[0])>2 else [1, "1"]
        
        if genminstr == "":
            genminstr = f"{genmin:<8.3f}"
        if genmaxstr == "":
            genmaxstr = f"{genmax:<8.3f}"
        if genincstr == "":
            genincstr = f"{geninc:<8.3f}"
        sgs = 'gen'
    if genmin > genmax:
        print ("*** Min generator > max")
        return

    score_type = args.scoretype
    if score_type == 'a':
        sts = '|e| ave'
    elif score_type == 'm':
        sts = '|e| max' 
    elif score_type == 'r':
        sts = 'RMS(e)'
    else:
        print ("*** -s argument must be a or m or r")
        return

    if len(args.nsteps) > 2:
        print ("*** -n takes 1 or 2 arguments")
        return
    nmin = args.nsteps[0]
    nmax = args.nsteps[1] if len(args.nsteps) == 2 else nmin
    if nmin > nmax:
        print ("*** Min nsteps > max")
        return

    frac = args.frac
    if args.opti and typ == 'l':
        print ("*** Cannot use -o with -g")
        return
    opti = args.opti
    if args.best and genmax > genmin and nmax > nmin:
        print ("*** Cannot use -b when varying both gen and n")
        return
    best = args.best
    tab = args.tabs
    sep = '\t' if tab else ' '
    under = args.under

    dets = args.details
    if re.search ('[^dn]', dets):
        print ("-d argument must be combination of d, n only")
        return

    nint = len(ints)
    
    # Print header
    print ("Linear scales" if typ!='e' else ("Equal divisions" if frac==1 else "Fractional equal divisions"))
    print (f"{'P =':>8s}{sep}{perstr:<8s}", end="")
    for ii in range(nint):
        istr =  '' if intstr[ii] == '' else f"({intstr[ii]})"
        print (f"{sep}{'#'+str(ii+1)+' =':>8s}{sep}{ints[ii]:>8.3f}{sep}{istr:<8s}", end="")
    if frac > 1:
        pstr =  '' if perstr == '' else f"({perstr})"
        print (f"{sep}{'':8s}{sep}{'Period =':>8s}{sep}{per:>8.3f}{sep}{pstr:<8s}", end="")
    print ()

    print (f"{'n':>8s}{sep}{sgs:>8s}", end="")
    for ii in range(nint):
        print (f"{sep}{sgs+'s':>8s}{sep}{'val':>8s}{sep}{'err':>8s}", end="")
    print (f"{sep}{sts:>8s}", end="")
    if frac > 1:
        print (f"{sep}{sgs+'s':>8s}{sep}{'val':>8s}{sep}{'err':>8s}", end="")
    print ()

    # Loop over scales
    scales = []
    gen = genmin
    while gen <= genmax:
        broke = False
        # If showing best and nmax > nmin, consider all scales from 1 so 'best so far'
        # status will not depend on starting value of n
        for ni in range(1 if best and nmax > nmin else nmin, nmax+1):
            if typ == 'l':
                scale = {"n": 0, "na": [], "ca": [], "ea": [], "sg": 0.0, "sc": 0.0,
                         "e": typ=='e',
                         "fr": frac,
                         "per": per}
                n = ni
                notes = []
                for nn in range(n):
                    notenn = (nn*gen)%per
                    if nn > 0 and (notenn < 1e-5 or notenn > per - 1e-5):
                        broke = True
                        break
                    notes.append([nn, notenn])
                if broke:
                    break
                notes.sort(key=lambda x: x[1])
                notes.append([0, per])
                for ii in range(nint):
                    i1 = ints[ii]
                    i2 = per - i1
                    scale["na"].append (999)
                    scale["ca"].append (999.)
                    scale["ea"].append (999.)                    
                    for nn in range(n):
                        if notes[nn+1][1] > i1:
                            nnn = nn if i1-notes[nn][1] < notes[nn+1][1]-i1 else nn+1
                            ea1 = notes[nnn][1] - i1
                            if abs(ea1) < abs(scale["ea"][-1]):
                                scale["na"][-1] = notes[nnn][0]
                                scale["ca"][-1] = notes[nnn][1]
                                scale["ea"][-1] = ea1
                            break
                    for nn in range(n):
                        if notes[nn+1][1] > i2:
                            nnn = nn if i2-notes[nn][1] < notes[nn+1][1]-i2 else nn+1
                            ea2 = notes[nnn][1] - i2
                            if abs(ea2) < abs(scale["ea"][-1]):
                                scale["na"][-1] = -notes[nnn][0]
                                scale["ca"][-1] = per-notes[nnn][1]
                                scale["ea"][-1] = ea2
                            break
                scale["n"] = n
                scale["sg"] = gen
                scale["sc"] = score_calc(score_type, scale["ea"])
                scales.append(scale)
            else:
                for nf in range(frac):
                    scale = {"n": 0, "na": [], "ca": [], "ea": [], "sg": 0.0, "sc": 0.0,
                             "e": typ=='e',
                             "fr": frac,
                             "per": per}
                    n = ni + nf/frac
                    score = 0
                    step = per / n
                    for ii in range(nint):
                        scale["na"].append (int (ints[ii] / step + 0.5))
                        scale["ca"].append (scale["na"][-1] * step)
                        scale["ea"].append (scale["ca"][-1] - ints[ii])
                    scale["n"] = n
                    scale["sg"] = step
                    scale["sc"] = score_calc(score_type, scale["ea"])
                    scales.append(scale)
        gen += geninc
    
    if best:
        # Sieve the results

        # If linear or fractional:
        # In any run with the same na, keep only the one with the lowest score provided it's lowest so far
        best_score = 9999
        scalesa = list(scales)
        scales = []
        scale_minscore = {}
        if typ == 'l' or frac > 1:
            nal = []
            for scale in scalesa:
                if scale["na"] != nal:
                    if scale_minscore != {} and scale_minscore["sc"] < best_score:
                        scales.append(scale_minscore)
                        best_score = scale_minscore["sc"]
                    scale_minscore = dict(scale)
                    nal = scale["na"]
                else:
                    if scale["sc"] < scale_minscore["sc"]:
                        scale_minscore = dict(scale)
            if scale_minscore != {} and scale_minscore["sc"] < best_score:
                scales.append(scale_minscore)
        else:
            for scale in scalesa:
                if scale["sc"] < best_score:
                    scales.append(scale)
                    best_score = scale["sc"]
            
    last_step = 0
    scalesa = list(scales)
    scales = []
    for scale in scalesa:
        if scale["n"] >= nmin and scale["sc"] < under:
            if (typ == 'e' and frac > 1) and opti:
                nint = len(ints)
                step = sum([ints[ii]*scale["na"][ii] for ii in range(nint)]) / \
                    sum([scale["na"][ii]**2 for ii in range(nint)])
                if step == last_step:
                    continue
                last_step = step
                scale["sg"] = step
                scale["ca"] = [scale["na"][ii] * step for ii in range(nint)]
                scale["ea"] = [scale["ca"][ii] - ints[ii] for ii in range(nint)]
                scale["sc"] = score_calc(score_type, scale["ea"])
                scale["n"] =  per/step
            scales.append(scale)
                                
    # Print results

    for jj in range(len(scales)):
        scale = scales[jj]
        score = scale["sc"]
        n = scale["n"]
        na = scale["na"]
        ca = scale["ca"]
        ea = scale["ea"]
        sg = scale["sg"]
        if n >= nmin and score < under:
            if typ == 'e':
                print (f"{n:8.3f}{sep}" if frac > 1 else f"{int(n):8d}{sep}", end="")
            else:
                print (f"{int(n):8d}{sep}", end="")

            print (f"{sg:8.3f}{sep}", end="")
            for ii in range(nint):
                print (f"{na[ii]:8d}{sep}{ca[ii]:8.3f}{sep}{ea[ii]:8.3f}{sep}", end="")
            print (f"{score:8.3f}", end="")

            if frac > 1:
                nap = int(n+.5)
                cap = nap * sg
                eap = cap - per
                print (f"{sep}{nap:8d}{sep}{cap:8.3f}{sep}{eap:8.3f}", end="")
            print ()

            scale_dets (scale, dets)
                
if __name__ == "__main__":
    main()

