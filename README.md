# bestintervals.py

This is a Python script for exploring approximations to just (or other) intervals in equal divisions of the octave (EDOs) and linear scales.

## Motivation

You can use [continued fractions to find EDOs](https://oeis.org/DUNNE/Temperament2x.PDF) that contain good approximations to a single interval such as a perfect fifth. To find EDOs that best approximate multiple intervals, such as perfect fifths and major and minor thirds, [some](https://www.semanticscholar.org/paper/Music-and-Ternary-Continued-Fractions-Barbour/e67f461a73452f9c6a6257e93b7b7d0bfe5b15f4) [authors](https://www.jstor.org/stable/2307936) have proposed algorithms that try to extend the continued fractions idea to the problem of approximating multiple values. But drawbacks of these approaches are that there is no simple and agreed-upon best approach to multiple values, and even with single values, continued fractions give some but not all good approximations.

An advantage of the continued fractions approach is that it points the way to good approximations without a lot of calculational effort. But it is no longer 1941, and calculational effort is not the burden it once was. Simple brute force searching is quick enough on a computer, and doesn't skip over good candidates.

## Script features

With this script you can:

* Search for EDOs and linear tunings that are optimal for approximating a given set of intervals.
* Specify those intervals as fractional or decimal frequency ratios, or cents.
* Specify the period and, for linear scales, the generator, again as fractional or decimal frequency ratios, or cents.
* Score each scale by the root mean square (RMS) error, average absolute error, or maximum absolute error.
* Show all scales in a range of lengths, or limit to:
    * Ones whose score improves on any of shorter length
	* Ones whose score lies below some threshold
	* Both.
* Investigate what Carlos calls scales with a fractional number of steps to the octave.
* Display results in human readable form, or tab separated for pasting into a spreadsheet.

## Dependencies

`math` and `argparse` libraries.

## Usage

The usage message reads:

```
usage: bestintervals.py [-h] [-n NSTEPS [NSTEPS ...]] [-i INTS [INTS ...]] [-p PERIOD] [-g GENERATOR [GENERATOR ...]] [-f FRAC] [-s SCORETYPE] [-b] [-u UNDER] [-o] [-d DETAILS]
                        [-t]

options:
  -h, --help            show this help message and exit
  -n NSTEPS [NSTEPS ...], --nsteps NSTEPS [NSTEPS ...]
                        Min [and max] steps per octave, def = 12 100
  -i INTS [INTS ...], --ints INTS [INTS ...]
                        Just intervals as fractions or decimals, def 3/2 5/4
  -p PERIOD, --period PERIOD
                        Period as fraction or decimals, def 2/1
  -g GENERATOR [GENERATOR ...], --generator GENERATOR [GENERATOR ...]
                        Generator min, [max, [increment]] as fraction or decimals for linear, omit for EDO
  -f FRAC, --frac FRAC  (ET only) Do fractional divisions, FRAC per unit (default 1)
  -s SCORETYPE, --scoretype SCORETYPE
                        Score type: 'a' for average of |err| or 'm' for max(|err|) or r for RMS(err) (def)
  -b, --best            Show only best-scoring so far
  -u UNDER, --under UNDER
                        Show only if score is under UNDER
  -o, --opti            Optimize fractional divisions
  -d DETAILS, --details DETAILS
                        Enable printing details about each scale: d = Diatonic scales info, n = List notes with their intervals
  -t, --tabs            Separate columns with tabs
```

If you run `$ python3 bestintervals.py` with no arguments, it will show you a table with a line for each EDO from 12 to 100 steps. Each line shows the step size, and the number of steps, value, and error (value minus target value) for the best approximations to a major third (frequency ratio 5/4) and a perfect fifth (3/2), and an overall score. 

![](output.png)

Change this behavior with command line arguments:

### Number of notes

You can change the range of number of notes with the `-n` option. Arguments are the minimum and maximum number of notes. Maximum must be >= minimum. If not specified, maximum = minimum.

```
$ python3 bestintervals.py -n 7 24
```

will show scales with from 7 to 24 (inclusive) notes.

### Interval

You can specify any one or more intervals with the `-i` option; for example

```
$ python3 bestintervals.py -i 3/2 5/4 6/5
```

will show results for minor thirds (6/5) as well as perfect fifths and major thirds. You can specify the intervals as fractional frequency ratios, as shown, or as decimal ratios, e.g. `-i 1.5r 1.25r 1.2r`, or in cents: `-i 701.955 386.314 315.641`. (Note the final `r` distinguishing ratios from cents values.) The default is `3/2 5/4`.

### Period

The period is the interval which is divided (for EDOs) or at which the scale repeats (for linear scales). The default is the usual value, 2/1 (octave), but you can set any interval you want with the `-p` option, for instance

```
$ python3 bestintervals.py -p 3.0r
```

for a period of a perfect twelfth (or "tritave", if you must). Again you can use a fractional ratio, decimal ratio, or value in cents.

### Generator

By default the script looks at EDOs, but if you specify a generator it will look at linear tunings. Use the `-g` option. Arguments are the minimum and maximum generator and the increment. Maximum must be >= minimum. If not specified, maximum = minimum. If not specified, increment = 1 cent.

```
$ python3 bestintervals.py -g 600 700 50 -n 10 12
```

This will analyze scales whose notes are generated by successively adding the generator mod the period. In this case it will try to generate scales of 10, 11, and 12 notes using generators 600, 650, and 700 cents. However, with 600 cents the maximum scale length is 2 (the 3rd note wraps around back to the starting note) so only 650 and 700 cent scales are shown.

Note that with linear scales, there may be a better approximation to an interval by going *down* some number of generators than going up. For example, in Pythagorean tuning, there is a better major third by going down 8 fifths than by going up 4 fifths. In such cases the number of generators displayed is negative.

### Fractional divisions

This is an idea that has been discussed by [Carlos](https://www.wendycarlos.com/resources/pitch.html). She refers to it as division of the octave into a fractional number of steps, which really doesn't make a lot of sense; a better way to express it is as equal division of a *tempered* octave. But her way of stating it does have the advantage that it relies on only a single continuously variable parameter — the fractional "number of divisions" — versus one continuous variable (the octave temperament) and one discrete variable (the integer number of divisions). So if the "number of divisions" is 18.2, then the step size is 1200/18.2 = 65.93 cents, and we can go ahead and figure out how many such steps give the best approximation for each interval.

If you run:

```
$ python3 bestintervals.py -f 10
```

then the "number of divisions" will increment by 1/10 instead of 1. Results look like the following. Note that to the right of the score are three additional columns giving the number of steps, value, and error for the best approximation to the period (which however is not included in the score — unless you specify it as one of the target intervals).

```
Fractional equal divisions
     P = 2/1          #1 =  701.955 (3/2)        #2 =  386.314 (5/4)             Period = 1200.000 (2/1)   
       n     step    steps      val      err    steps      val      err   RMS(e)    steps      val      err
  12.000  100.000        7  700.000   -1.955        4  400.000   13.686    6.913       12 1200.000    0.000
  12.100   99.174        7  694.215   -7.740        4  396.694   10.381    6.474       12 1190.083   -9.917
  12.200   98.361        7  688.525  -13.430        4  393.443    7.129    7.603       12 1180.328  -19.672
  12.300   97.561        7  682.927  -19.028        4  390.244    3.930    9.715       12 1170.732  -29.268
  12.400   96.774        7  677.419  -24.536        4  387.097    0.783   12.274       12 1161.290  -38.710
  12.500   96.000        7  672.000  -29.955        4  384.000   -2.314   15.022       13 1248.000   48.000
...
```

The `-f` option cannot be used in conjunction with the `-g` option.

### Score type

By default the score for a scale is the root mean square of the errors. Use `-s a` to instead display the average of the absolute values of the errors, or `-s m` for the maximum absolute value of the error. `-s r` gives the default RMS.

### Best-scoring mode

If you use the `-b` option, not all scales will be displayed: Only ones whose score is lower than all previous scores, and, in the case of fractional divisions or linear scales, only the lowest scoring scale with the same number of steps or generators for every interval.

`-b` cannot be used if both the number of notes and the generator are being allowed to vary.

### Maximum-scoring mode

Use `-u` to set a threshold under which the score must fall for the scale to be printed:

```
$ python3 bestintervals.py -u 1.5
```

prints only `n = 53, 65, 84, 87, 96, 99` because they are the only ones with RMS error under 1.5.

You can specify both `-b` and `-u`, and only scales meeting *both* criteria will be printed.

### Optimization

When using fractional divisions, specifying `-o` tells the script not to print each scale as analyzed, but to run an optimization which varies the step size to minimize the mean square error. The resulting scale is what is printed, unless the optimized value matches that of the previous printed scale. The above fractional division results when optimized become:

```
$ python3 bestintervals.py -f 10 -o
Fractional equal divisions
     P = 2/1          #1 =  701.955 (3/2)        #2 =  386.314 (5/4)             Period = 1200.000 (2/1)   
       n     step    steps      val      err    steps      val      err   RMS(e)    steps      val      err
  12.076   99.368        7  695.578   -6.377        4  397.473   11.160    6.426       12 1192.420   -7.580
  13.406   89.511        8  716.089   14.134        4  358.045  -28.269   15.803       13 1163.645  -36.355
  14.151   84.800        8  678.401  -23.554        5  424.000   37.687   22.221       14 1187.201  -12.799
  15.420   77.822        9  700.401   -1.554        5  389.111    2.798    1.600       15 1167.334  -32.666
  16.758   71.609       10  716.089   14.134        5  358.045  -28.269   15.803       17 1217.352   17.352
  17.478   68.658       10  686.576  -15.379        6  411.946   25.632   14.946       17 1167.179  -32.821
...
```

Combined with `-b` one gets:

```
$ python3 bestintervals.py -f 10 -o -b
Fractional equal divisions
     P = 2/1          #1 =  701.955 (3/2)        #2 =  386.314 (5/4)             Period = 1200.000 (2/1)   
       n     step    steps      val      err    steps      val      err   RMS(e)    steps      val      err
  12.076   99.368        7  695.578   -6.377        4  397.473   11.160    6.426       12 1192.420   -7.580
  15.420   77.822        9  700.401   -1.554        5  389.111    2.798    1.600       15 1167.334  -32.666
  18.766   63.945       11  703.397    1.442        6  383.671   -2.643    1.505       19 1214.958   14.958
  34.185   35.103       20  702.056    0.101       11  386.131   -0.183    0.104       34 1193.495   -6.505
```

where the last three of these match (well enough) Carlos's alpha, beta, and gamma scales. When using optimization, I find the results generally don't change using larger `-f` values than 10. 

You cannot use the `-o` option in conjunction with the `-g` option.

### Details

You can get details about each scale using the `d` option. The argument is a string which must contain `d`, `n`, or both.

`d` means to print details about diatonic scales. For EDO, any number of steps totalling in the range 1200×4/7 to 1200×3/5 cents can be used to generate diatonic scales, and information like the following will be shown:

```
Diatonic scale with generator =  7 steps 700.000 cents
Diatonic semitone =     1 steps  100.000 cents
Chromatic semitone =    1 steps  100.000 cents
Whole tone =            2 steps  200.000 cents
Diatonic m3 =           3 steps  300.000 cents err =  -15.641 cents
Diatonic M3 =           4 steps  400.000 cents err =   13.686 cents
Diatonic P5 =           7 steps  700.000 cents err =   -1.955 cents
```

The errors for m3, M3, and P5 are differences between the diatonic interval and the pure interval. For linear tunings, only a diatonic scale based on the tuning's generator will be considered, and only if it is in the 1200×4/7 to 1200×3/5 cents range.

'n' means to print all the notes in the scale, for example:

```
 step     Diff    Cents   Name   Intervals
    0  100.000    0.000    C    
    1  100.000  100.000    C♯    dia semi, chr semi
    2  100.000  200.000    D     dia whole, 8/7 (-31.174)
    3  100.000  300.000    E♭    dia m3, m3 (-15.641), 7/6 (33.129), n3 (-47.408)
    4  100.000  400.000    E     dia M3, M3 (13.686)
    5  100.000  500.000    F     dia P4, P4 (1.955)
    6  100.000  600.000    F♯    7/5 (17.488), 10/7 (-17.488)
    7  100.000  700.000    G     dia P5, P5 (-1.955)
    8  100.000  800.000    A♭    dia m6, m6 (-13.686)
    9  100.000  900.000    A     dia M6, M6 (15.641), 12/7 (-33.129), n6 (47.408)
   10  100.000 1000.000    B♭    7/4 (31.174)
   11  100.000 1100.000    B     dia LT
```

Columns are the number of steps (for EDO, or generators for linear) for the note; difference in cents between this note and the previous one (or the first note and one period below the last); name of the note (with C as 0), provided a diatonic scale is possible; and intervals from C approximated by each note, with errors in the case of pure intervals. If an EDO allows for more than one diatonic scale, this list will be repeated for each.

All these details will be shown for *each* EDO or linear tuning printed, so `-d` is best used in combination with arguments that don't print too many scales!

### Tab separator

Use `-t` to change the column separators in the output to tabs, so it can be copied and pasted into a spreadsheet. Here's a Google Sheets chart I made that way, showing the RMS error versus fractional division; you can see the sharp minima near 15.4, 18.8, and 34.2:

![](FracDivRMS.png)

